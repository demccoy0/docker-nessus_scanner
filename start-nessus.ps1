Param(
    [securestring]$AdminPass = (Read-Host -AsSecureString -Prompt "Please enter an admin password for Nessus"),
    [String]$License
)

$cred = New-Object PSCredential -ArgumentList 'admin',$AdminPass
docker run -p 8834:8834 -e LICENSE=$License -e ADMIN_PASS=$($cred.GetNetworkCredential().Password) -dt stevemcgrath/nessus_scanner:latest -P